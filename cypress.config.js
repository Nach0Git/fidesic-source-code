const { defineConfig } = require('cypress')

module.exports = defineConfig({
  projectId: '21gawa',
  defaultCommandTimeout: 20000,
  numTestsKeptInMemory: 20,
  retries: 2,
  requestTimeout: 10000,
  videoUploadOnPasses: false,
  viewportWidth: 1280,
  viewportHeight: 800,
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    baseUrl: 'https://dev.fidesic.com/',
    excludeSpecPattern: '**/SkippedTestFiles/*.js',
    specPattern: 'cypress/e2e/**/*.{js,jsx,ts,tsx}',
  },
})
