#   R e a d   m e  
  
 -   T h i s   p r o j e c t   a l l o w s   d e v e l o p m e n t   o f   t h e   f r o n t   e n d   s e p e r a t e   f r o m   t h e   w h o l e   F i d e s i c   p r o j e c t   w i t h o u t   n e e d i n g   t o   r u n   t h e   b a c k e n d   s e r v e r  
 -   T h i s   r u n s   a g a i n s t   o u r   t e s t i n g   e n v i r o n m e n t   ( S t a g i n g . f i d e s i c . c o m )  
  
 # #   S e t u p   A n g u l a r J S   l o c a l   d e v e l o p m e n t  
  
 -   M a k e   s u r e   n o d e / n p m   i s   i n s t a l l e d  
 -   r u n   n p m   i n s t a l l   t o   i n s t a l l   d e p e n d e n c i e s  
 -   i n   t h e   r o o t   f o l d e r ,   r u n   n p x   w s   - - s p a   v 2 / i n d e x . h t m l   - - h t t p s   - - p o r t   4 4 3  
 -   Y o u   c a n   n o w   a c c e s s   F i d e s i c   a t   h t t p s : / / 1 2 7 . 0 . 0 . 1 : 4 4 3    
  
 # #   S e t u p   c y p r e s s  
 -   O p e n   t h e   f o l d e r   t h a t   c o n t a i n s   t h e   c y p r e s s   f o l d e r  
 -   R u n   n p x   c y p r e s s   o p e n   - - e n v   a c c o u n t _ b u i l d = < n u m b e r >  
 -   < n u m b e r >   c a n   b e   a n y t h i n g ,   i t ' l l   j u s t   b e   t h e   n a m e   o f   t h e   c o m p a n y   y o u ' r e   t e s t i n g   a g a i n s t  
 -   E a c h   f o l d e r   u n d e r   e 2 e   r e q u i r e s   a   c o m p a n y   s e t u p   t o   r u n   t h o s e   t e s t s  
 -   E x :   A P   f o l d e r   t e s t s   r e q u i r e   A P A c c o u n t S e t u p   t o   r u n   t o   s e t u p   t h e   d a t a 